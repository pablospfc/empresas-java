## Sobre a API

Este projeto é uma API desenvolvida como parte do teste para Desenvolvedor Java Spring Boot da Ioasys. É uma API para para cadastro de filmes. Foi desenvolvida utilizando Java com Spring Boot. A URL da api é /api-ioasys.

## Funcionalidades

Os endpoints providos por esta API são:

* Criar Novo Filme: `POST /filme`
* Listar Filmes: `GET /filme`  
* Cadastrar Usuário: `POST /usuario`
* Remover Usuário: `DELETE /usuario/:id`
* Editar Usuário: `PUT /usuario/:id`  
* Listar Usuários: `GET /usuario`  
* Votar: `POST /voto`
* Autenticar: `POST /login`

### Detalhes dos Endpoints

`POST /filme`

Este endpoint é utilizado para criar um novo filme.

**Payload body:**

```json
{
  "nome": "A Rua do Medo",
  "genero": "Terror",
  "diretor":"Leigh Janiak",
  "atores": [
    {
      "nome": "Sadie Sink"
    },
    {
      "nome": "Emily Rudd"
    }
  ]
}
```


`GET /filme/?page=0&limit=10&name=nomefilme`

Este endpoint retorna uma lista de filmes. Aceita algums parâmetros:

`nome` - nome do filme

`diretor` - diretor do filme

`genero` - genero do filme

`page` -  página

`limit` -  limite por página


`POST /usuario`

Este endpoint é utilizado para criar novos usuários.

**Payload body:**

```json
{
  "nome": "Claudio Pablo",
  "login": "claudio_pablo",
  "senha": "12qwaszx",
  "idPerfil": 2
}
```


`DELETE /usuario/:id`

Este endpoint faz uma remoção lógica de um determinado usuário especificado a partir do id informado.

**Onde:**

`id` - id do usuário desejado.


`PUT /usuario/:id`

Este endpoint é utilizado para atualizar um determinado usuário.

**Payload body:**

```json
{
  "nome": "Claudio Pablo",
  "login": "claudio_pablo",
  "senha": "12qwaszx",
  "idPerfil": 2
}
```

**Onde:**

`id` - id do usuário desejado.


`GET /usuario`

Este endpoint retorna uma lista de usuários. Aceita algums parâmetros:

`page` -  página

`limit` -  limite por página


`POST /voto`

Este endpoint é utilizado para criar um voto.

**Payload body:**

```json
{
  "idFilme": 2,
  "idUsuario": 2,
  "valor": 3
}
```


`POST /login`

Este endpoint é utilizado para que o usuário possa se autenticar na api.

**Payload body:**

```json
{
  "idFilme": 2,
  "idUsuario": 2,
  "valor": 3
}
```

Obs: As rotas que requerem autenticação, deverão ser chamadas através do Authentication Bearer para serem executadas.

### Tecnologias Utilizadas

Este projeto foi desenvolvida com as ferramentas:

* **Java 8**
* **Maven**
* **Swagger**
* **Mysql 8**
* **Junit**


### Execução

Para executar a API, simplesmente execute:

```bash
mvn spring-boot:run
```

Por padrão, a API estará disponível em [http://localhost:8081](http://localhost:8081)

### Documentação

* Swagger (somente ambiente de desenvolvimento): [http://localhost:8081/swagger-ui.html](http://localhost:8081/swagger-ui.html)



