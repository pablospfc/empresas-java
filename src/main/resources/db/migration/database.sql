-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ioasys
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ioasys
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ioasys` DEFAULT CHARACTER SET utf8 ;
USE `ioasys` ;

-- -----------------------------------------------------
-- Table `ioasys`.`filme`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ioasys`.`filme` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `diretor` VARCHAR(45) NULL,
  `genero` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ioasys`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ioasys`.`usuario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NOT NULL,
  `login` VARCHAR(255) NOT NULL,
  `senha` VARCHAR(255) NOT NULL,
  `conta_nao_expirada` BIT(1) NULL,
  `conta_nao_bloqueada` BIT(1) NULL,
  `credenciais_nao_expirada` BIT(1) NULL,
  `ativo` BIT(1) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ioasys`.`voto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ioasys`.`voto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_usuario` INT NOT NULL,
  `id_filme` INT NOT NULL,
  `valor` INT NULL,
  INDEX `fk_voto_usuario_idx` (`id_usuario` ASC) VISIBLE,
  INDEX `fk_voto_filme1_idx` (`id_filme` ASC) VISIBLE,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_voto_usuario`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `ioasys`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_voto_filme1`
    FOREIGN KEY (`id_filme`)
    REFERENCES `ioasys`.`filme` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ioasys`.`permissao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ioasys`.`permissao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ioasys`.`ator`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ioasys`.`ator` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ioasys`.`ator_filme`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ioasys`.`ator_filme` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_ator` INT NOT NULL,
  `id_filme` INT NOT NULL,
  INDEX `fk_ator_has_filme_filme1_idx` (`id_filme` ASC) VISIBLE,
  INDEX `fk_ator_has_filme_ator1_idx` (`id_ator` ASC) VISIBLE,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_ator_has_filme_ator1`
    FOREIGN KEY (`id_ator`)
    REFERENCES `ioasys`.`ator` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ator_has_filme_filme1`
    FOREIGN KEY (`id_filme`)
    REFERENCES `ioasys`.`filme` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ioasys`.`usuario_permissao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ioasys`.`usuario_permissao` (
  `id_usuario` INT NOT NULL,
  `id_permissao` INT NOT NULL,
  PRIMARY KEY (`id_usuario`, `id_permissao`),
  INDEX `fk_usuario_has_permissao_permissao1_idx` (`id_permissao` ASC) VISIBLE,
  INDEX `fk_usuario_has_permissao_usuario1_idx` (`id_usuario` ASC) VISIBLE,
  CONSTRAINT `fk_usuario_has_permissao_usuario1`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `ioasys`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_has_permissao_permissao1`
    FOREIGN KEY (`id_permissao`)
    REFERENCES `ioasys`.`permissao` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
