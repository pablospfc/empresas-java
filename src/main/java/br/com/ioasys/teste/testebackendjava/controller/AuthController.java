package br.com.ioasys.teste.testebackendjava.controller;

import br.com.ioasys.teste.testebackendjava.dto.LoginDTO;
import br.com.ioasys.teste.testebackendjava.jwt.JwtTokenProvider;
import br.com.ioasys.teste.testebackendjava.repository.UsuarioRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/login")
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UsuarioRepository usuarioRepository;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager,
                          JwtTokenProvider jwtTokenProvider,
                          UsuarioRepository usuarioRepository) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.usuarioRepository = usuarioRepository;
    }

    @ApiOperation("Login do Usuário")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Usuário autenticado"),
            @ApiResponse(code = 400, message = "Entrada inválida"),
            @ApiResponse(code = 403, message = "Dados do login inválidos"),
            @ApiResponse(code = 500, message = "Ocorreu alguma exceção na aplicação")
    })
    @PostMapping(produces = {"application/json","application/xml", "application/x-yaml"},
            consumes =  {"application/json","application/xml", "application/x-yaml"})
    public ResponseEntity<?> login(@RequestBody LoginDTO usuarioDTO) {

        try {
            var username = usuarioDTO.getLogin();
            var password = usuarioDTO.getSenha();

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

            var user = usuarioRepository.findByLogin(username);

            var token = "";

            if (user != null) {
                token = jwtTokenProvider.createToken(username, user.getRoles());
            } else {
                throw new UsernameNotFoundException("Usuário não encontrado");
            }

            Map<Object, Object> model = new HashMap<Object, Object>();
            model.put("login", username);
            model.put("token", token);

            return ResponseEntity.ok(model);

        }catch (AuthenticationException ex) {
            throw new BadCredentialsException(String.format("Usuário ou senha inválidos: %s", ex.getMessage()));
        }
    }
}
