package br.com.ioasys.teste.testebackendjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Usuario implements UserDetails, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;

    private String login;

    private String senha;

    @Column(name = "conta_nao_expirada")
    private Boolean contaNaoExpirada;

    @Column(name = "conta_nao_bloqueada")
    private Boolean contaNaoBloqueada;

    @Column(name = "credenciais_nao_expirada")
    private Boolean credenciaisNaoExpirada;

    private Boolean ativo;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "usuario_permissao", joinColumns = { @JoinColumn(name = "id_usuario")}, inverseJoinColumns = {@JoinColumn(name="id_permissao")})
    private List<Permissao> permissoes;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        List<String> permissoes = this.getRoles();
        permissoes.forEach(p -> {
            GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + p);
            authorities.add(authority);
        });
        return authorities;
    }

    public List<String> getRoles() {
        List<String> roles = new ArrayList<>();
        this.permissoes.stream()
                .forEach(p-> {
                    roles.add(p.getDescricao());
                });
        return roles;
    }

    @Override
    public String getPassword() {
        return senha;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return contaNaoExpirada;
    }

    @Override
    public boolean isAccountNonLocked() {
        return contaNaoBloqueada;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credenciaisNaoExpirada;
    }

    @Override
    public boolean isEnabled() {
        return ativo;
    }
}
