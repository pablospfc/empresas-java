package br.com.ioasys.teste.testebackendjava.repository;

import br.com.ioasys.teste.testebackendjava.dto.ListFilmesDTO;
import br.com.ioasys.teste.testebackendjava.entity.Ator;
import br.com.ioasys.teste.testebackendjava.entity.Filme;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.annotation.security.PermitAll;
import java.util.List;

@Repository
public interface FilmeRepository extends JpaRepository<Filme, Long> {

    @Query("select  new br.com.ioasys.teste.testebackendjava.dto.ListFilmesDTO(f.nome," +
            "       f.genero," +
            "       f.diretor," +
            "       f.id, " +
            "       avg(v.valor)) " +
            "from Voto v " +
            "left join Filme f on f.id = v.idFilme.id " +
            "WHERE 1=1 " +
            "            and (:nome IS NULL OR f.nome LIKE %:nome%) " +
            "             and (:diretor IS NULL OR f.diretor LIKE %:diretor%) " +
            "             and (:genero IS NULL OR f.genero LIKE %:genero%) " +
            "group by f.id, f.nome")
    Page<ListFilmesDTO> list(@Param("nome") String nome,
                             @Param("diretor") String diretor,
                             @Param("genero") String genero,
                             Pageable pageable);

    Filme findByNome(String nome);
}
