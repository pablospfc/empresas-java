package br.com.ioasys.teste.testebackendjava.repository;

import br.com.ioasys.teste.testebackendjava.dto.UsuarioDTO;
import br.com.ioasys.teste.testebackendjava.entity.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    @Query("SELECT u FROM Usuario u WHERE u.login =:login")
    Usuario findByLogin(@Param("login") String login);

    @Query("SELECT new br.com.ioasys.teste.testebackendjava.dto.UsuarioDTO(u.nome, u.login, p.id) FROM Usuario u " +
            "inner join u.permissoes p " +
            "where p.id = 2 and u.ativo = true " +
            "order by u.nome asc")
    Page<UsuarioDTO> findUsersNonAdmin(Pageable pageable);
}
