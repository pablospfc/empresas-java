package br.com.ioasys.teste.testebackendjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
public class TesteBackendJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(TesteBackendJavaApplication.class, args);
    }

}
