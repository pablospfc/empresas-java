package br.com.ioasys.teste.testebackendjava.repository;

import br.com.ioasys.teste.testebackendjava.entity.Ator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AtorRepository extends JpaRepository<Ator, Long> {

    Ator findByNome(String nome);
}
