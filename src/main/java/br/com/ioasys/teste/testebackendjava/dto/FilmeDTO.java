package br.com.ioasys.teste.testebackendjava.dto;

import br.com.ioasys.teste.testebackendjava.entity.Ator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

public class FilmeDTO {

    private String nome;
    private String genero;
    private String diretor;
    private List<AtorDTO> atores;

    public FilmeDTO() {
    }

    public FilmeDTO(String nome, String genero, String diretor) {
        this.genero = genero;
        this.nome = nome;
        this.diretor = diretor;
    }

    public FilmeDTO(String nome, String genero, String diretor, List<AtorDTO> atores) {
        this.nome = nome;
        this.genero = genero;
        this.diretor = diretor;
        this.atores = atores;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDiretor() {
        return diretor;
    }

    public void setDiretor(String diretor) {
        this.diretor = diretor;
    }

    public List<AtorDTO> getAtores() {
        return atores;
    }

    public void setAtores(List<AtorDTO> atores) {
        this.atores = atores;
    }
}
