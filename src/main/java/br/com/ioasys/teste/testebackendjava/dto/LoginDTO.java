package br.com.ioasys.teste.testebackendjava.dto;

import lombok.*;

import java.io.Serializable;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class LoginDTO implements Serializable {

    private String login;

    private String senha;
}
