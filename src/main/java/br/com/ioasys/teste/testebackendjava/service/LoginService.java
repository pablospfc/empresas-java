package br.com.ioasys.teste.testebackendjava.service;

import br.com.ioasys.teste.testebackendjava.repository.PermissaoRepository;
import br.com.ioasys.teste.testebackendjava.repository.UsuarioRepository;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class LoginService implements UserDetailsService {

    public final UsuarioRepository usuarioRepository;

    private final PermissaoRepository permissaoRepository;

    public LoginService(UsuarioRepository usuarioRepository,
                        PermissaoRepository permissaoRepository) {
        this.usuarioRepository = usuarioRepository;
        this.permissaoRepository = permissaoRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // TODO Auto-generated method stub
        var user = usuarioRepository.findByLogin(username);

        if (user != null) {
            return user;
        } else {
            throw new UsernameNotFoundException("Login" + username + " não encontrado");
        }
    }
}
