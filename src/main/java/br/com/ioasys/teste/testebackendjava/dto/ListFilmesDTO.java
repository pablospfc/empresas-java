package br.com.ioasys.teste.testebackendjava.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

public class ListFilmesDTO extends FilmeDTO {

    private Long id;
    private Double mediaVotos;

    public ListFilmesDTO(String nome, String genero, String diretor, Long id, Double mediaVotos) {
        super(nome, genero, diretor);
        this.id = id;
        this.mediaVotos = mediaVotos;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getMediaVotos() {
        return mediaVotos;
    }

    public void setMediaVotos(Double mediaVotos) {
        this.mediaVotos = mediaVotos;
    }
}
