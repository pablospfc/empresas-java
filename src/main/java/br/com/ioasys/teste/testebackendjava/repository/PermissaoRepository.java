package br.com.ioasys.teste.testebackendjava.repository;

import br.com.ioasys.teste.testebackendjava.entity.Filme;
import br.com.ioasys.teste.testebackendjava.entity.Permissao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissaoRepository extends JpaRepository<Permissao, Long> {

    @Query("SELECT p FROM Permissao p WHERE p.descricao =:descricao")
    Permissao findByDescription(@Param("descricao") String descricao);
}
