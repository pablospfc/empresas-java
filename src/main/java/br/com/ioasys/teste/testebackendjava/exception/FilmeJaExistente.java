package br.com.ioasys.teste.testebackendjava.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class FilmeJaExistente extends Exception {

    public FilmeJaExistente(String message) {
        super(message);
    }
}
