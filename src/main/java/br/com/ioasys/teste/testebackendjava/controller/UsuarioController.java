package br.com.ioasys.teste.testebackendjava.controller;

import br.com.ioasys.teste.testebackendjava.dto.UsuarioDTO;
import br.com.ioasys.teste.testebackendjava.exception.UserNotFoundException;
import br.com.ioasys.teste.testebackendjava.service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.net.URISyntaxException;

@Api(value = "Usuario")
@RestController
@RequestMapping(value = "/usuario")
@CrossOrigin
public class UsuarioController {

    private final UsuarioService usuarioService;

    @Autowired
    public UsuarioController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @ApiOperation("Salva um usuário")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Usuário salvo"),
            @ApiResponse(code = 400, message = "Entrada inválida"),
            @ApiResponse(code = 500, message = "Ocorreu alguma exceção na aplicação")
    })
    @PostMapping
    public ResponseEntity<?> save(@RequestBody UsuarioDTO usuarioDTO) throws URISyntaxException {
        usuarioService.save(usuarioDTO);
        return ResponseEntity.created(new URI("/usuario")).build();
    }

    @ApiOperation("Listagem de usuários")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Lista de usuários"),
            @ApiResponse(code = 400, message = "Entrada inválida"),
            @ApiResponse(code = 500, message = "Ocorreu alguma exceção na aplicação")
    })
    @GetMapping
    public ResponseEntity<Page<UsuarioDTO>> list(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                                 @RequestParam(value = "limit", defaultValue = "10") Integer limit) {
        Pageable pageable = PageRequest.of(page, limit);
        return new ResponseEntity<>(usuarioService.getUsersNonAdmin(pageable),HttpStatus.OK);
    }

    @ApiOperation("Remoção Lógica de Usuário")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Removido"),
            @ApiResponse(code = 400, message = "Entrada inválida"),
            @ApiResponse(code = 404, message = "Usuário não encontrado"),
            @ApiResponse(code = 500, message = "Ocorreu alguma exceção na aplicação")
    })
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> disable(@PathVariable("id") Long id ) throws UserNotFoundException {
        usuarioService.desativar(id);
        return ResponseEntity.ok().build();
    }

    @ApiOperation("Edição de Usuário")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Usuário atualizado"),
            @ApiResponse(code = 400, message = "Entrada inválida"),
            @ApiResponse(code = 404, message = "Usuário não encontrado"),
            @ApiResponse(code = 500, message = "Ocorreu alguma exceção na aplicação")
    })
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody UsuarioDTO usuarioDTO, @PathVariable("id") Long id) throws UserNotFoundException {
        usuarioService.update(usuarioDTO, id);
        return ResponseEntity.ok().build();
    }


}
