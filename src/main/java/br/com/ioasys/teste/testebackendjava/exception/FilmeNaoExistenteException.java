package br.com.ioasys.teste.testebackendjava.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class FilmeNaoExistenteException extends Exception {

    public FilmeNaoExistenteException(String message) {
        super(message);
    }
}
