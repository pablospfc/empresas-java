package br.com.ioasys.teste.testebackendjava.service;

import br.com.ioasys.teste.testebackendjava.dto.UsuarioDTO;
import br.com.ioasys.teste.testebackendjava.entity.Permissao;
import br.com.ioasys.teste.testebackendjava.entity.Usuario;
import br.com.ioasys.teste.testebackendjava.exception.UserNotFoundException;
import br.com.ioasys.teste.testebackendjava.repository.PermissaoRepository;
import br.com.ioasys.teste.testebackendjava.repository.UsuarioRepository;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    public final UsuarioRepository usuarioRepository;

    private final PermissaoRepository permissaoRepository;

    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UsuarioService(UsuarioRepository usuarioRepository,
                          PermissaoRepository permissaoRepository,
                          BCryptPasswordEncoder passwordEncoder) {
        this.usuarioRepository = usuarioRepository;
        this.permissaoRepository = permissaoRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public Page<UsuarioDTO> getUsersNonAdmin(Pageable pageable) {
        return usuarioRepository.findUsersNonAdmin(pageable);
    }

    public void desativar(Long id) throws UserNotFoundException {
        Optional<Usuario> usuario = usuarioRepository.findById(id);

        if (!usuario.isPresent()) {
            throw new UserNotFoundException("Usuário não encontrado");
        }

        Usuario user = usuario.get();
        user.setAtivo(false);
        usuarioRepository.save(user);
    }

    public void save(UsuarioDTO usuarioDTO) {
        Permissao permissao = null;
        Optional<Permissao> findPermission = permissaoRepository.findById(usuarioDTO.getIdPerfil());

        permissao = findPermission.get();

        Usuario usuario = new Usuario();
        usuario.setNome(usuarioDTO.getNome());
        usuario.setLogin(usuarioDTO.getLogin());
        usuario.setSenha(passwordEncoder.encode(usuarioDTO.getSenha()));
        usuario.setContaNaoExpirada(true);
        usuario.setContaNaoBloqueada(true);
        usuario.setCredenciaisNaoExpirada(true);
        usuario.setAtivo(true);

        usuario.setPermissoes(Arrays.asList(permissao));

        Usuario find = usuarioRepository.findByLogin(usuarioDTO.getLogin());
        if (find == null) {
            usuarioRepository.save(usuario);
        }
    }

    public void update(UsuarioDTO usuarioDTO, Long id) throws UserNotFoundException {
        Optional<Usuario> usuario = usuarioRepository.findById(id);

        if (!usuario.isPresent()) {
            throw new UserNotFoundException("Usuário não encontrado");
        }

        Usuario user = usuario.get();
        user.setNome(usuarioDTO.getNome());
        user.setLogin(usuarioDTO.getLogin());
        
        usuarioRepository.save(user);
    }
}
