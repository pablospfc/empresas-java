package br.com.ioasys.teste.testebackendjava.controller;

import br.com.ioasys.teste.testebackendjava.dto.FilmeDTO;
import br.com.ioasys.teste.testebackendjava.dto.ListFilmesDTO;
import br.com.ioasys.teste.testebackendjava.entity.Filme;
import br.com.ioasys.teste.testebackendjava.exception.FilmeJaExistente;
import br.com.ioasys.teste.testebackendjava.service.FilmeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(value = "Filme")
@RestController
@RequestMapping(value = "/filme")
public class FilmeController {

    @Autowired
    private FilmeService filmeService;

    @ApiOperation("Salva um filme")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Filme salvo"),
            @ApiResponse(code = 400, message = "Filme já existente"),
            @ApiResponse(code = 500, message = "Ocorreu alguma exceção na aplicação")
    })
    @PostMapping
    public ResponseEntity<?> save(@RequestBody FilmeDTO filmeDTO) throws FilmeJaExistente {
        filmeService.save(filmeDTO);
        return ResponseEntity.ok().build();
    }

    @ApiOperation("Listagem de filmes")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Lista de filmes"),
            @ApiResponse(code = 400, message = "Entrada inválida"),
            @ApiResponse(code = 500, message = "Ocorreu alguma exceção na aplicação")
    })
    @GetMapping
    public ResponseEntity<Page<ListFilmesDTO>> list(@RequestParam(value = "nome", required = false) String nome,
                                                   @RequestParam(value = "diretor",required = false) String diretor,
                                                   @RequestParam(value = "genero", required = false) String genero,
                                                   @RequestParam(value = "page", defaultValue = "0") Integer page,
                                                   @RequestParam(value = "limit", defaultValue = "10") Integer limit) {

        Pageable pageable = PageRequest.of(page, limit);

        return new ResponseEntity<Page<ListFilmesDTO>>(filmeService.list(nome,diretor,genero, pageable),HttpStatus.OK);
    }


}
