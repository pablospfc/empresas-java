package br.com.ioasys.teste.testebackendjava.dto;

import br.com.ioasys.teste.testebackendjava.entity.Permissao;
import lombok.*;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UsuarioDTO {

    private String nome;
    private String login;
    private String senha;
    private Long idPerfil;

    public UsuarioDTO(String nome, String login, Long idPerfil) {
        this.nome = nome;
        this.login = login;
        this.idPerfil = idPerfil;
    }
}
