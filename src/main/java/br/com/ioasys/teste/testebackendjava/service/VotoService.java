package br.com.ioasys.teste.testebackendjava.service;

import br.com.ioasys.teste.testebackendjava.dto.VotoDTO;
import br.com.ioasys.teste.testebackendjava.entity.Filme;
import br.com.ioasys.teste.testebackendjava.entity.Usuario;
import br.com.ioasys.teste.testebackendjava.entity.Voto;
import br.com.ioasys.teste.testebackendjava.exception.FilmeNaoExistenteException;
import br.com.ioasys.teste.testebackendjava.exception.UserNotFoundException;
import br.com.ioasys.teste.testebackendjava.repository.FilmeRepository;
import br.com.ioasys.teste.testebackendjava.repository.UsuarioRepository;
import br.com.ioasys.teste.testebackendjava.repository.VotoRepository;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class VotoService {

    private final VotoRepository votoRepository;
    private final FilmeRepository filmeRepository;
    private final UsuarioRepository usuarioRepository;

    public VotoService(VotoRepository votoRepository,
                       FilmeRepository filmeRepository,
                       UsuarioRepository usuarioRepository) {
        this.votoRepository = votoRepository;
        this.filmeRepository = filmeRepository;
        this.usuarioRepository = usuarioRepository;
    }

    public void save(VotoDTO votoDTO) throws FilmeNaoExistenteException, UserNotFoundException {
        Voto voto = new Voto();
        Optional<Filme> filmeOptional = Optional.ofNullable(filmeRepository.findById(votoDTO.getIdFilme()).orElseThrow(() -> new FilmeNaoExistenteException("O filme não existe")));
        Optional<Usuario> usuarioOptional = Optional.ofNullable(usuarioRepository.findById(votoDTO.getIdUsuario()).orElseThrow(() -> new UserNotFoundException("O Usuário não existe")));

        Filme filme = filmeOptional.get();
        Usuario usuario =  usuarioOptional.get();

        voto.setIdFilme(filme);
        voto.setIdUsuario(usuario);
        voto.setValor(votoDTO.getValor());

        votoRepository.save(voto);
    }
}
