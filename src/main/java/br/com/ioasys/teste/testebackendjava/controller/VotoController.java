package br.com.ioasys.teste.testebackendjava.controller;

import br.com.ioasys.teste.testebackendjava.dto.VotoDTO;
import br.com.ioasys.teste.testebackendjava.entity.Voto;
import br.com.ioasys.teste.testebackendjava.exception.FilmeNaoExistenteException;
import br.com.ioasys.teste.testebackendjava.exception.UserNotFoundException;
import br.com.ioasys.teste.testebackendjava.service.VotoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.net.URI;
import java.net.URISyntaxException;

@Api(value = "Voto")
@RestController
@RequestMapping(value = "/voto")
public class VotoController {

    @Autowired
    private VotoService votoService;

    @ApiOperation("Salva um voto")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Retorna o novo voto salvo"),
            @ApiResponse(code = 400, message = "Entrada inválida"),
            @ApiResponse(code = 404, message = "Filme ou Usuário inexistentes"),
            @ApiResponse(code = 500, message = "Ocorreu alguma exceção na aplicação")
    })
    @PostMapping
    @PreAuthorize("hasRole('PUBLICO')")
    public ResponseEntity<?> save(@RequestBody VotoDTO votoDTO) throws FilmeNaoExistenteException, UserNotFoundException, URISyntaxException {
        votoService.save(votoDTO);
        return ResponseEntity.created(new URI("/voto")).build();
    }


}
