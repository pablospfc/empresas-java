package br.com.ioasys.teste.testebackendjava.service;

import br.com.ioasys.teste.testebackendjava.dto.AtorDTO;
import br.com.ioasys.teste.testebackendjava.dto.FilmeDTO;
import br.com.ioasys.teste.testebackendjava.dto.ListFilmesDTO;
import br.com.ioasys.teste.testebackendjava.entity.Ator;
import br.com.ioasys.teste.testebackendjava.entity.Filme;
import br.com.ioasys.teste.testebackendjava.exception.FilmeJaExistente;
import br.com.ioasys.teste.testebackendjava.repository.AtorRepository;
import br.com.ioasys.teste.testebackendjava.repository.FilmeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class FilmeService {

    private final FilmeRepository filmeRepository;
    private final AtorRepository atorRepository;


    @Autowired
    public FilmeService(FilmeRepository filmeRepository, AtorRepository atorRepository) {
        this.filmeRepository = filmeRepository;
        this.atorRepository = atorRepository;
    }

    @Transactional
    public Filme save(FilmeDTO filmeDTO) throws FilmeJaExistente {
        Filme filme = filmeRepository.findByNome(filmeDTO.getNome());

        if (Objects.nonNull(filme)) {
            throw new FilmeJaExistente("Filme já existe");
        }

        List<Ator> atores = new ArrayList<>();
        filme = buildFilme(filmeDTO);
        filme = filmeRepository.save(filme);

        filmeDTO.getAtores().forEach(f -> {
            Ator ator = atorRepository.findByNome(f.getNome());

            if (Objects.isNull(ator)) {
                ator = new Ator();
                ator.setNome(f.getNome());
            }
            atores.add(ator);
        });

        filme.setAtores(atores);
        atorRepository.saveAll(atores);

        return filme;
    }

    public Page<ListFilmesDTO> list(String nome, String diretor, String genero, Pageable pageable) {
        Page<ListFilmesDTO> filmes =  filmeRepository.list(nome, diretor, genero, pageable);
        filmes.forEach(f-> {
            Filme filme = filmeRepository.findById(f.getId()).get();
            List<Ator> atores = filme.getAtores();
            List<AtorDTO> atoresDTO = buildAtorDTO(atores);
            f.setAtores(atoresDTO);

        });

        return filmes;
    }

    private List<AtorDTO> buildAtorDTO(List<Ator> atores) {
        List<AtorDTO> atoresDTO = new ArrayList<>();

        atores.forEach(a-> {
            AtorDTO atorDTO = new AtorDTO();
            atorDTO.setNome(a.getNome());
            atoresDTO.add(atorDTO);
        });
        return atoresDTO;
    }

    private Filme buildFilme(FilmeDTO filmeDTO) {
        Filme filme = new Filme();

        filme.setNome(filmeDTO.getNome());
        filme.setGenero(filmeDTO.getGenero());
        filme.setDiretor(filmeDTO.getDiretor());

        return filme;
    }

    public Optional<Filme> getById(Long id) {
        return filmeRepository.findById(id);
    }

    public Filme update(Filme newFilm, Long id) {
        Filme filme = filmeRepository.findById(id).get();

        filme.setDiretor(newFilm.getDiretor());
        filme.setGenero(newFilm.getGenero());
        filme.setNome(newFilm.getNome());

        filmeRepository.saveAndFlush(filme);

        return filme;
    }

    public void delete(Long id) {
        filmeRepository.deleteById(id);
    }
}
