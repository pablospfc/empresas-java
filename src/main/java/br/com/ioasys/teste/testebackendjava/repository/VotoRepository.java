package br.com.ioasys.teste.testebackendjava.repository;

import br.com.ioasys.teste.testebackendjava.entity.Voto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VotoRepository extends JpaRepository<Voto, Long> {
}
