package br.com.ioasys.teste.testebackendjava.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Filme {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name="ator_filme",
            joinColumns={@JoinColumn(name="id_filme")},
            inverseJoinColumns={@JoinColumn(name="id_ator")})
    private List<Ator> atores;

    private String diretor;

    private String genero;
}
