package br.com.ioasys.teste.testebackendjava.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VotoDTO {

    @JsonProperty(value = "idUsuario")
    private Long idUsuario;
    @JsonProperty(value = "idFilme")
    private Long idFilme;
    @JsonProperty(value = "valor")
    private Integer valor;
}
