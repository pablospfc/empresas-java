package br.com.ioasys.teste.testebackendjava.integration;

import br.com.ioasys.teste.testebackendjava.dto.AtorDTO;
import br.com.ioasys.teste.testebackendjava.dto.FilmeDTO;
import br.com.ioasys.teste.testebackendjava.dto.UsuarioDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    private HttpEntity<Void> protectedHeader;
    private HttpEntity<Void> adminHeader;
    private HttpEntity<Void> wrongHeader;


    @Test
    @Order(1)
    public void configProtectedHeaders() {
        String str = "{\"login\":\"pablospfc\",\"senha\":\"45enef80khs1\"}";
        HttpHeaders headers = restTemplate.postForEntity("/api-ioasys/login", str, String.class).getHeaders();
        this.protectedHeader = new HttpEntity<>(headers);
    }

    @Test
    @Order(2)
    public void saveFilmeTest() {
        FilmeDTO filmeDTO = new FilmeDTO();
        filmeDTO.setNome("A Rua do Medo");
        filmeDTO.setDiretor("Leigh Janiak");
        filmeDTO.setGenero("Terror");
        List<AtorDTO> atores = Arrays.asList(new AtorDTO("Sadie Sink"), new AtorDTO("Emily Rudd"));
        filmeDTO.setAtores(atores);
        final HttpEntity<FilmeDTO> entity = new HttpEntity<>(filmeDTO);

        ResponseEntity<String> responseEntity = this.restTemplate
                .exchange("http://localhost:" + port + "/api-ioasys/filme", HttpMethod.POST, new HttpEntity<>(entity,protectedHeader.getHeaders()), String.class );

        Assert.assertEquals(201, responseEntity.getStatusCodeValue());
    }

    @Test
    @Order(3)
    public void listFilmeTest() {
        ResponseEntity<String> responseEntity = this.restTemplate.exchange(
                "http://localhost:" + port + "/api-ioasys/filme?page=0&limit=10", HttpMethod.GET, null, String.class);

        Assert.assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    @Order(4)
    public void saveUsuarioTest() {
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        usuarioDTO.setNome("A Rua do Medo");
        usuarioDTO.setLogin("userioasys");
        usuarioDTO.setSenha("12qwaszx");
        usuarioDTO.setIdPerfil(1L);

        final HttpEntity<UsuarioDTO> entity = new HttpEntity<>(usuarioDTO);

        ResponseEntity<String> responseEntity = this.restTemplate
                .exchange("http://localhost:" + port + "/api-ioasys/usuario", HttpMethod.POST, entity, String.class );

        Assert.assertEquals(201, responseEntity.getStatusCodeValue());
    }

}
