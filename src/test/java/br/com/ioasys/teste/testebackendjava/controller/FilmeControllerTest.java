package br.com.ioasys.teste.testebackendjava.controller;

import br.com.ioasys.teste.testebackendjava.dto.AtorDTO;
import br.com.ioasys.teste.testebackendjava.dto.FilmeDTO;
import br.com.ioasys.teste.testebackendjava.dto.ListFilmesDTO;
import br.com.ioasys.teste.testebackendjava.entity.Ator;
import br.com.ioasys.teste.testebackendjava.entity.Filme;
import br.com.ioasys.teste.testebackendjava.exception.FilmeJaExistente;
import br.com.ioasys.teste.testebackendjava.service.FilmeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class FilmeControllerTest {

    @Mock
    private FilmeService filmeService;

    @InjectMocks
    private FilmeController filmeController;

    @Test
    public void deveRetornar201AoSalvar() throws FilmeJaExistente {
        FilmeDTO filmeSalvo = new FilmeDTO();
        filmeSalvo.setNome("A Rua do Medo");
        filmeSalvo.setDiretor("Leigh Janiak");
        filmeSalvo.setGenero("Terror");
        List<AtorDTO> atoresSalvos = Arrays.asList(new AtorDTO("Sadie Sink"), new AtorDTO("Emily Rudd"));
        filmeSalvo.setAtores(atoresSalvos);

        Filme filmeEsperado = new Filme();
        filmeEsperado.setNome("A Rua do Medo");
        filmeEsperado.setDiretor("Leigh Janiak");
        filmeEsperado.setGenero("Terror");
        List<Ator> atoresEsperados = Arrays.asList(new Ator(1L, Collections.singletonList(filmeEsperado), "Sadie Sink"), new Ator(2L, Collections.singletonList(filmeEsperado),"Emily Rudd"));
        filmeEsperado.setAtores(atoresEsperados);

        Mockito.when(filmeService.save(filmeSalvo)).thenReturn(filmeEsperado);
        ResponseEntity<?> response = filmeController.save(filmeSalvo);
        Assert.assertTrue(response.getStatusCode().is2xxSuccessful());
    }

    @Test
    public void deveRetornar400CasoFilmeNaoExista() throws FilmeJaExistente {
        Filme filmeExistente = new Filme();
        filmeExistente.setId(1L);
        filmeExistente.setNome("A Rua do Medo");
        filmeExistente.setDiretor("Leigh Janiak");
        filmeExistente.setGenero("Terror");

        FilmeDTO filmeASalvar = new FilmeDTO();
        filmeASalvar.setNome("A Rua do Medo");
        filmeASalvar.setDiretor("Leigh Janiak");
        filmeASalvar.setGenero("Terror");
        List<AtorDTO> atoresSalvos = Arrays.asList(new AtorDTO("Sadie Sink"), new AtorDTO("Emily Rudd"));
        filmeASalvar.setAtores(atoresSalvos);

        Mockito.when(filmeService.save(filmeASalvar)).thenThrow(new FilmeJaExistente("Filme já existe"));
        ResponseEntity<?> response = filmeController.save(filmeASalvar);

        Assert.assertTrue(response.getStatusCode().is4xxClientError());
    }

}
